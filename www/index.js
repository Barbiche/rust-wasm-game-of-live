import { UniverseKind, Universe, Cell } from "wasm-game-of-life/wasm_game_of_life";
import { memory } from "wasm-game-of-life/wasm_game_of_life_bg";
import * as THREE from 'three';

const scene = new THREE.Scene();

const CELL_SIZE = 5; // px
const GRID_COLOR = "#CCCCCC";
const DEAD_COLOR = "#FFFFFF";
const ALIVE_COLOR = "#DC143C";

const universe = Universe.new();
universe.fill_default();
const width = universe.width();
const height = universe.height();

const canvas = document.getElementById("game-of-life-canvas");
canvas.height = (CELL_SIZE + 1) * height + 1;
canvas.width = (CELL_SIZE + 1) * width + 1;

const canvasThree = document.getElementById("game-of-life-canvas-three");
canvasThree.width = 768;
canvasThree.height = 768;

const texture = new THREE.CanvasTexture(canvas);
const material = new THREE.MeshBasicMaterial({
    map: texture
});
const cube = new THREE.Mesh(new THREE.BoxGeometry(400, 400, 400), material);
const renderer = new THREE.WebGLRenderer({antialias: true, canvas: canvasThree});
renderer.setSize(768, 768);
const camera = new THREE.PerspectiveCamera(75, 1, 0.1, 1000);
camera.position.z = 600;
scene.add(cube);

const ctx = canvas.getContext('2d');
const playPauseButton = document.getElementById("play-pause");
const resetButton = document.getElementById("reset");

let animationId = null;

const fps = new class {
    constructor() {
        this.fps = document.getElementById("fps");
        this.frames = [];
        this.lastFrameTimeStamp = performance.now();
    }

    render(){
        const now = performance.now();
        const delta = now - this.lastFrameTimeStamp;
        this.lastFrameTimeStamp = now;
        const fps = 1 / delta * 1000;

        // Save only the latest 100 timings.
        this.frames.push(fps);
        if (this.frames.length > 100) {
            this.frames.shift();
        }

        let min = Infinity;
        let max = -Infinity;
        let sum = 0;
        for (let i = 0; i < this.frames.length; i++) {
            sum += this.frames[i];
            min = Math.min(this.frames[i], min);
            max = Math.max(this.frames[i], max);
        }
        let mean = sum / this.frames.length;

        // Render stats
        this.fps.textContent = `
        Frames per Second:
        latest = ${Math.round(fps)}
        avg of last 100 = ${Math.round(mean)}
        min of last 100 = ${Math.round(min)}
        max of last 100 = ${Math.round(max)}
        `.trim();
    }
};

let getRangeTick = () => {
    return document.getElementById("rangeTick").value;
}

function renderLoop (time) {
    fps.render();
    
    drawCells();

    let rangeTick = getRangeTick();
    for (let index = 0; index < rangeTick; index++) {
        universe.tick();
    }

    // Three rendering
    renderThree(time);

    animationId = requestAnimationFrame(renderLoop);
};

const isPaused = () => {
    return animationId === null;
};

const play = () => {
    playPauseButton.textContent = "⏸";
    renderLoop();
};

const pause = () => {
    playPauseButton.textContent = "▶";
    cancelAnimationFrame(animationId);
    animationId = null;
};

resetButton.addEventListener("click", () => {
    universe.reset();
    universe.fill_default();
    drawGrid();
    drawCells();
});

playPauseButton.addEventListener("click", () => {
    if (isPaused()) {
        play();
    } else {
        pause();
    }
});

const drawGrid = () => {
    ctx.beginPath();
    ctx.strokeStyle = GRID_COLOR;

    for (let i = 0; i <= width; i++) {
        ctx.moveTo(i * (CELL_SIZE+1) + 1, 0);
        ctx.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * height + 1);
    }

    for (let i = 0; i <= width; i++) {
        ctx.moveTo(0, i * (CELL_SIZE + 1) + 1);
        ctx.lineTo((CELL_SIZE + 1) * width + 1, i * (CELL_SIZE + 1) + 1);
    }

    ctx.stroke();
};

const getIndex = (row, column) => {
    return row * width + column;
};

const drawCells = () => {
    const cellsPtr = universe.cells();
    const cells = new Uint8Array(memory.buffer, cellsPtr, width * height);

    ctx.beginPath();

    for (let row = 0; row < height; row++) {
        for (let col = 0; col < width; col++) {
            const idx = getIndex(row, col);

            ctx.fillStyle = cells[idx] === Cell.Dead ? DEAD_COLOR : ALIVE_COLOR;
            ctx.fillRect(col * (CELL_SIZE + 1) + 1, row * (CELL_SIZE + 1) + 1, CELL_SIZE, CELL_SIZE);
        }
    }

    ctx.stroke();
};

const renderThree = (time) => {
    time *= 0.0005;  // convert time to seconds
 
    cube.rotation.y = time;
    cube.rotation.x = time;
    texture.needsUpdate = true;
    renderer.render(scene, camera);
};

canvas.addEventListener("click", event => {
   const boundingRect = canvas.getBoundingClientRect();
   
   const scaleX = canvas.width / boundingRect.width;
   const scaleY = canvas.height / boundingRect.height;

   const canvasLeft = (event.clientX - boundingRect.left) * scaleX;
   const canvasTop = (event.clientY - boundingRect.top) * scaleY;

   const row = Math.min(Math.floor(canvasTop / (CELL_SIZE + 1)), height - 1);
   const col = Math.min(Math.floor(canvasLeft / (CELL_SIZE + 1)), width - 1);

   if (event.ctrlKey)
   {
        universe.add_spaceship(row, col);
   } else if (event.shiftKey)
   {
        universe.add_pulsar(row, col);
   } else {
        universe.toggle_cell(row, col);
   }

   drawCells();
});

drawCells();

play();

renderer.render(scene, camera);

