mod utils;

use wasm_bindgen::prelude::*;
use std::fmt;

extern crate js_sys;
extern crate web_sys;
use web_sys::console;


pub struct Timer<'a> {
    name: &'a str,
}

impl<'a> Timer<'a> {
    pub fn new(name: &'a str) -> Timer<'a> {
        console::time_with_label(name);
        Timer { name }
    }
}

impl<'a> Drop for Timer<'a> {
    fn drop(&mut self) {
        console::time_end_with_label(self.name);
    }
}


#[wasm_bindgen]
#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Cell {
    Dead = 0,
    Alive = 1,
}

#[wasm_bindgen]
pub struct Universe {
    width: u32,
    height: u32,
    cells: Vec<Cell>,
}

impl Cell {
    fn toggle(&mut self) {
        *self = match *self {
            Cell::Dead => Cell::Alive,
            Cell::Alive => Cell::Dead,
        };
    }
}

impl Universe {
    fn get_index(&self, row: u32, column: u32) -> usize {
        (row * self.width + column) as usize
    }

    fn live_neighbor_count(&self, row: u32, column: u32) -> u8 {
        let mut count = 0;

        let north = if row == 0 {
            self.height - 1
        } else {
            row - 1
        };

        let south = if row == self.height - 1 {
            0
        } else {
            row + 1
        };

        let west = if column == 0 {
            self.width - 1
        } else {
            column - 1
        };
    
        let east = if column == self.width - 1 {
            0
        } else {
            column + 1
        };

        let nw = self.get_index(north, west);
        count += self.cells[nw] as u8;
    
        let n = self.get_index(north, column);
        count += self.cells[n] as u8;
    
        let ne = self.get_index(north, east);
        count += self.cells[ne] as u8;
    
        let w = self.get_index(row, west);
        count += self.cells[w] as u8;
    
        let e = self.get_index(row, east);
        count += self.cells[e] as u8;
    
        let sw = self.get_index(south, west);
        count += self.cells[sw] as u8;
    
        let s = self.get_index(south, column);
        count += self.cells[s] as u8;
    
        let se = self.get_index(south, east);
        count += self.cells[se] as u8;
    
        count
    }

    pub fn get_cells(&self) -> &[Cell] {
        &self.cells
    }

    pub fn set_cells(&mut self, cells: &[(u32, u32)]) {
        for (row, col) in cells.iter().cloned() {
            let idx = self.get_index(row, col);
            self.cells[idx] = Cell::Alive;
        }
    }
}

#[wasm_bindgen]
impl Universe {
    pub fn tick(&mut self) {
        //let _timer = Timer::new("Universe::tick");
        let mut next = self.cells.clone();

        for row in 0..self.height {
            for col in 0..self.width {
                let idx = self.get_index(row, col);
                let cell = self.cells[idx];
                let live_neighbors = self.live_neighbor_count(row, col);
                let next_cell = match (cell, live_neighbors) {
                    (Cell::Alive, x) if x < 2 => Cell::Dead,
                    (Cell::Alive, 2) | (Cell::Alive, 3) => Cell::Alive,
                    (Cell::Alive, x) if x > 3 => Cell::Dead,
                    (Cell::Dead, 3) => Cell::Alive,
                    (otherwise, _) => otherwise,
                };

                next[idx] = next_cell;
            }
        }

        self.cells = next;
    }

    pub fn new() -> Universe {
        utils::set_panic_hook();

        let width = 128;
        let height = 128;

        Universe { width: width, height: height, cells: (0..width*height).map(|_| Cell::Dead).collect() }
    }

    pub fn render(&self) -> String {
        self.to_string()
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn cells(&self) -> *const Cell {
        self.cells.as_ptr()
    }

    /// Fill the universe with random alive cells.
    pub fn fill_random(&mut self)
    {
        for cell in self.cells.iter_mut()
        {
            if js_sys::Math::random() < 0.5 {
                *cell = Cell::Alive;
            } else {
                *cell = Cell::Dead;
            }
        }
    }

    pub fn fill_default(&mut self)
    {
        for (i, cell) in self.cells.iter_mut().enumerate()
        {
            if i % 2 == 0 || i % 7 == 0 {
                *cell = Cell::Alive;
            } else {
                *cell = Cell::Dead
            }
        }
    }

    pub fn add_pulsar(&mut self, row: u32, column: u32)
    {      
        if row == 0 || row == self.width {
            return;
        }

        self.toggle_cell(row, column);
        self.toggle_cell(row+1, column);
        self.toggle_cell(row-1, column);
    }

    pub fn add_spaceship(&mut self, row: u32, column: u32)
    {
        let start_index = self.get_index(row, column);
        let mut id = 0;
        id += start_index;
        
        // ◼
        // ◻◻◻◻◻
        id += 5;
        id += (self.width-5) as usize;

        // ◻◻◻◼◻
        id += 4;
        self.cells[id] = Cell::Alive;
        id +=1;
        id += (self.width-5) as usize;

        // ◻◼◻◼◻
        id += 2;
        self.cells[id] = Cell::Alive;
        id +=2;
        self.cells[id] = Cell::Alive;
        id +=1;
        id += (self.width-5) as usize;

        // ◻◻◼◼◻
        id += 3;
        self.cells[id] = Cell::Alive;
        id +=1;
        self.cells[id] = Cell::Alive;
    }

    /// Reset all cells to Dead value.
    pub fn reset(&mut self)
    {
        self.cells =(0..self.width * self.height).map(|_| Cell::Dead).collect();
    }

    pub fn set_width(&mut self, width: u32) {
        self.width = width;
        self.reset();
    }

    pub fn set_height(&mut self, height: u32) {
        self.height = height;
        self.reset();
    }

    pub fn toggle_cell(&mut self, row: u32, column: u32) {
        let idx = self.get_index(row, column);
        self.cells[idx].toggle();
    }
}

impl fmt::Display for Universe {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        for line in self.cells.as_slice().chunks(self.width as usize) {
            for &cell in line {
                let symbol = if cell == Cell::Dead { '◻' } else { '◼'};
                write!(f, "{}", symbol)?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}